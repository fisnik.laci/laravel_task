## Requirements
- php 7.2 + 
- php extensions
- mysql

## Setup Instructions
- git clone https://gitlab.com/fisnik.laci/laravel_task.git
- cd laravel_task
- composer install
- cp .env.example .env
- Create database and set credentials in .env file
- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan storage:link
- php artisan serve
