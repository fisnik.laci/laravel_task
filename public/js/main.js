$(function () {
    /**
     * @author Fisnik Laci
     * Get products filtered by category
     */
    $(document).on('click', '.category__js', function (event) {
        event.preventDefault();
        let route = $(this).attr('data-attr-action');
        let category_id = $(this).attr('data-attr-category-id');
        if ((route === undefined || route === '') || (category_id === undefined || category_id === '')) {
            new Noty({
                type: 'error',
                layout: 'bottomRight',
                text: 'Please choose a category!',
                timeout: 5000,
                closeWith: ['button', 'click'],
            }).show();
            return 0;
        }
        /* Ajax Request Header setup */
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        /* Submit form data using ajax */
        $.ajax({
            url: route,
            method: 'GET',
            data: {
            },
            processData: false,
            contentType: false,
            dataType: 'html',
            beforeSend: function() {
                $(this).attr('disabled' ,true);
            },
            success: function(response) {
                let products = $('.products__js');
                $('.nav-link').removeClass('active');
                $('#category-'+category_id+'').addClass('active');
                products.html(response);
            },
            error: function (error) {
                let message = '';
                if (error.status === 404) {
                    message = 'Something went wrong. Please try again!';
                } else {
                    message = error.responseText;
                }
                new Noty({
                    type: 'error',
                    layout: 'bottomRight',
                    text: message,
                    timeout: 5000,
                    closeWith: ['button', 'click'],
                }).show();
            },
            complete: function () {
                $(this).attr('disabled' ,false);
            }
        });
    });
});
