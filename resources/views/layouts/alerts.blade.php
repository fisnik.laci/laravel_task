@if(session('success'))
    <script type="text/javascript">
        new Noty({
            type: 'error',
            layout: 'bottomRight',
            text: '{{ session('success') }}',
            timeout: 5000,
            closeWith: ['button', 'click'],
        }).show();
    </script>
@endif
@if(session('info'))
    <script type="text/javascript">
        new Noty({
            type: 'error',
            layout: 'bottomRight',
            text: '{{ session('info') }}',
            timeout: 5000,
            closeWith: ['button', 'click'],
        }).show();
    </script>
@endif
@if(session('warning'))
    <script type="text/javascript">
        new Noty({
            type: 'error',
            layout: 'bottomRight',
            text: '{{ session('warning') }}',
            timeout: 5000,
            closeWith: ['button', 'click'],
        }).show();
    </script>
@endif
@if(session('error'))
    <script type="text/javascript">
        new Noty({
            type: 'error',
            layout: 'bottomRight',
            text: '{{ session('error') }}',
            timeout: 5000,
            closeWith: ['button', 'click'],
        }).show();
    </script>
@endif
