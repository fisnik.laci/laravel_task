<header class="header fixed-top">
    <div class="logo text-center justify-content-center align-items-center py-4">
        <a href="{{ route('home') }}">
            <h2>PHP <span class="custom-text">TASK</span></h2>
        </a>
    </div>

    {{-- Navbar --}}
    @includeWhen(\Illuminate\Support\Facades\Route::currentRouteName() === 'home', 'layouts.navbar')
    {{-- Navbar --}}
</header>
