<div class="container-fluid bg-purple-light py-2">
    <div class="container">
        @if($categories->isNotEmpty())
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand d-lg-none" href="#">Menu</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        @foreach($categories as $category)
                            <li class="nav-item py-2 py-lg-0 text-center">
                                <a class="nav-link custom-nav-link category__js" id="category-{{ $category->id }}" href="javascript:void(0)" data-attr-category-id="{{ $category->id }}" data-attr-action="{{ route('category', ['category' => $category]) }}">{{ $category->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </nav>
        @endif
    </div>
</div>
