@if($products->isNotEmpty())
    @foreach($products as $product)
        <div class="col-md-4">
            <div class="card mb-4 box-shadow">
                <img class="card-img-top"
                     data-src="{{ asset('storage/'.$product->image.'') }}"
                     alt="{{ $product->title }}"
                     src="{{ asset('storage/'.$product->image.'') }}"
                     data-holder-rendered="true">
                <div class="card-body py-5">
                    <a href="{{ route('product', ['product' => $product]) }}">
                        <h5 class="card-title">{{ $product->title }}</h5>
                    </a>
                    <p class="card-text">
                        {{ \Illuminate\Support\Str::limit($product->description, 80) }}
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                        <a class="category__js" href="javascript:void(0)" data-attr-category-id="{{ $product->category->id }}" data-attr-action="{{ route('category', ['category' => $product->category]) }}">
                            <small class="font-weight-bold">{{ $product->category->title }}</small>
                        </a>
                        <small class="font-weight-bold">{{ $product->price }} €</small>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="container">
        <div class="row">
            {{ $products->links() }}
        </div>
    </div>
@endif

