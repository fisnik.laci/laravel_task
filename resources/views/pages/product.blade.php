@extends('root')
@section('content')
<div class="container product-details">
    <div class="row py-5">
        <div class="col-md-5 offset-md-2 pt-3 order-md-2">
            <div class="product-img">
                <img src="{{ asset('storage/'.$product->image.'') }}"
                     alt="{{ $product->title }}">
            </div>
        </div>
        <div class="col-md-5 order-md-1">
            <h3>{{ $product->title }}</h3>
            <p class="mb-3 text-muted text-uppercase small">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            <p><span class="mr-1 mb-3"><strong>$ {{ $product->price }}</strong></span></p>
            <button type="button" class="btn btn-primary mb-4">Add to Cart</button>
            <p><span class="mr-1"><strong>{{ $product->category->title }}</strong></span></p>
            <p class="">{{ $product->description }}</p>
        </div>
    </div>
</div>
@endsection
