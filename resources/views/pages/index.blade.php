@extends('root')
@section('content')
    <div class="products bg-light">
        <div class="container">
            <div class="row products__js">
                @include('partials.products')
            </div>
        </div>
    </div>
@endsection
