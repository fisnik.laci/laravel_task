<!DOCTYPE html>
<html lang="en">

{{-- Head --}}
@include('layouts.head')
{{-- Head --}}

<body>

{{-- Header --}}
@include('layouts.header')
{{-- Header --}}

{{-- Content --}}
@yield('content')
{{-- Content --}}

{{-- Scripts --}}
@include('layouts.scripts')
{{-- Scripts --}}

{{-- Alerts --}}
@include('layouts.alerts')
{{-- Alerts --}}

</body>
</html>
