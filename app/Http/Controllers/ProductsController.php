<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use \Illuminate\Contracts\Foundation\Application;
use \Illuminate\Contracts\View\Factory;
use \Illuminate\View\View;

class ProductsController extends Controller
{
    /**
     * ProductsController constructor.
     */
    public function __construct()
    {

    }

    /**
     * @author Fisnik Laci
     * @return Application|Factory|View
     */
    public function home() {
        $products = Product::paginate(6);
        $categories = Category::all();
        return view('pages.index', compact('products', 'categories'));
    }

    /**
     * @author Fisnik Laci
     * @param Request $request
     * @param Product $product
     * @return Application|Factory|\Illuminate\Http\RedirectResponse|View
     * Show a single product
     */
    public function product(Request $request, Product $product) {
        return view('pages.product', compact('product'));
    }

    /**
     * @author Fisnik Laci
     * @param Request $request
     * @param Category $category
     * @return Application|\Illuminate\Contracts\Routing\ResponseFactory|Factory|\Illuminate\Http\Response|View
     * Filter products based on category
     */
    public function category(Request $request, Category $category) {
        try {
            if ($request->ajax()) {
                if ($category->exists()) {
                    $products = Product::where('category_id', $category->id)->paginate(6);
                    return view('partials.products', compact('products'));
                }
                throw new \Exception('Category does not exists !', 403);
            }
            throw new \Exception('Something went wrong! Please try again or contact our support!', 403);
        } catch (\Exception $exception) {
            return response($exception->getMessage(),$exception->getCode());
        }
    }
}
