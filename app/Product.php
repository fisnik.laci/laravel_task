<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @var string[] $fillable
     */
    protected $fillable = [
        'title',
        'image',
        'price',
        'description',
        'category_id'
    ];

    /**
     * Relation with categories
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category() {
        return $this->belongsTo(Category::class,'category_id', 'id');
    }
}
